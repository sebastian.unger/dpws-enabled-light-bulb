package org.ws4d.wscompactsecurity.authentication;


import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.incubation.Types.SecurityTokenServiceType;
import org.ws4d.java.incubation.wscompactsecurity.securitytokenservice.DefaultSecurityTokenService;
import org.ws4d.wscompactsecurity.authentication.brokered.ValidateOperation;
import org.ws4d.wscompactsecurity.authentication.eccdh.ECC_DH1;
import org.ws4d.wscompactsecurity.authentication.eccdh.ECC_DH2;

public class SecurityTokenService extends DefaultSecurityTokenService {

	public SecurityTokenService() {

		super(SecurityTokenServiceType.ENDPOINT, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);

		/* ECC-DH */
		this.addOperation(new ECC_DH1());
		this.addOperation(new ECC_DH2());

		this.addOperation(new ValidateOperation());

	}

}
