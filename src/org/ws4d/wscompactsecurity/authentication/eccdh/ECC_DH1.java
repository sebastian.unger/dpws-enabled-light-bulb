package org.ws4d.wscompactsecurity.authentication.eccdh;

import org.ws4d.java.incubation.wscompactsecurity.authentication.eccdh.DefaultEndpointECC_DH1;
import org.ws4d.java.incubation.wscompactsecurity.authentication.types.ServiceAuthenticationCallbacks;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;
import org.ws4d.mobile.authentication.flicker.BitKeyTool;
import org.ws4d.mobile.authentication.flicker.FramboosKeyOutputDriver;
import org.ws4d.mobile.authentication.lightbulb.BulbHandler;

import framboos.OutPin;

public class ECC_DH1 extends DefaultEndpointECC_DH1{

	ServiceAuthenticationCallbacks sab = null;
	private OutPin mOutPin = null;

	public ECC_DH1() {
		super(null);
		BulbHandler h = BulbHandler.getInstance();
		this.mOutPin = (h==null) ? null : h.getBulbPin();
		sab = new ServiceAuthenticationCallbacks() {
			@Override
			public void populatePin(int pin, QName mechanism) {
				populateOOBSharedSecret(pin);
			}

			@Override
			public void authenticationResult(EndpointReference ep,
					boolean success) {

			}
		};
		setServiceAuthenticationCallback(sab);
	}

	private void populateOOBSharedSecret(int pin) {
		// TODO: Some magic to happen here...
		Log.info(" =================================================== ");
		Log.info(" ===    PIN: " + pin);
		Log.info(" =================================================== ");

		BitKeyTool bkt = new BitKeyTool(new FramboosKeyOutputDriver(this.mOutPin));
		//FIXME: Should be configurable
		bkt.getDriver().setSymbolWidth(450);
		bkt.intToBool(pin);
		bkt.driveKeyOut();
	}



}
