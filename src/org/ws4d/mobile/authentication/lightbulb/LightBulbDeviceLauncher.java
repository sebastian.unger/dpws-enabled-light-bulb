package org.ws4d.mobile.authentication.lightbulb;

import java.io.IOException;

import org.ws4d.java.JMEDSFramework;
import org.ws4d.java.incubation.Types.SecurityAlgorithmSet;
import org.ws4d.java.incubation.WSSecurityForDevices.AlgorithmConstants;
import org.ws4d.java.incubation.WSSecurityForDevices.WSSecurityForDevicesConstants;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authorization.engine.AuthorizationEngine;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;
import org.ws4d.wscompactsecurity.authentication.SecurityTokenService;

public class LightBulbDeviceLauncher {

	static {
		AuthenticationEngine.setDefaultOwnerID("urn:uuid:19fc6dc0-e254-11e2-80c3-01cd7c7b068b");
	}

	public static void main(String[] args) {
		BulbHandler.init(0);
		ButtonHandler.init(1);

		Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
		Log.setShowTimestamp(true);

		JMEDSFramework.start(args);
		LightBulbDevice device = new LightBulbDevice();
		final LightBulbService service = new LightBulbService();

		service.addOperation(new LightBulbState());

		service.addOperation(new LightBulbSwitch());

		device.addService(service);

		/* Security-related Setup */
		AuthenticationEngine ae = AuthenticationEngine.getInstance();
		ae.setDbName("/home/pi/keys");
		ae.loadSecurityAssociations();

		/* add supported authentication mechanisms */
		ae.addSupportedOOBauthenticationMechanism(new QName(WSSecurityForDevicesConstants.BrokeredAuthentication, WSSecurityForDevicesConstants.NAMESPACE, WSSecurityForDevicesConstants.NAMESPACEprefix));
		ae.addSupportedOOBauthenticationMechanism(new QName(WSSecurityForDevicesConstants.FlickerAuthentication, WSSecurityForDevicesConstants.NAMESPACE, WSSecurityForDevicesConstants.NAMESPACEprefix));

		/* add supported security algorithms */
		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_AES_CBC128),
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_ENC_AES_CBC128)
						)
				);
		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_RC4),
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_ENC_RC4)
						)
				);
		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_AES_CBC128),
						null
						)
				);
		ae.addSupportedAlgorithmsSet(
				new SecurityAlgorithmSet(
						(QName) AlgorithmConstants.algorithmString.get(AlgorithmConstants.ALG_SIG_SHA1_RC4),
						null
						)
				);

		device.addService(new SecurityTokenService());

		AuthorizationEngine aze = AuthorizationEngine.getInstance();
		aze.loadDatabase();

		try {
			device.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
