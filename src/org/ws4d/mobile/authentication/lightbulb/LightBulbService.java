package org.ws4d.mobile.authentication.lightbulb;

import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.service.DefaultService;
import org.ws4d.java.types.URI;

public class LightBulbService extends DefaultService {
	
	public LightBulbService() {
		super(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		this.setServiceId(new URI("http://www.ws4d.org/LightBulbService"));
	}
	
	

}
