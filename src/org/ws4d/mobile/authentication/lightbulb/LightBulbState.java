package org.ws4d.mobile.authentication.lightbulb;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.schema.Type;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;

public class LightBulbState extends Operation {

	public LightBulbState() {
		
		super("LightBulbState", new QName("LightBulbService", "http://www.ws4d.org/"));
		
		Type xsString = SchemaUtil.TYPE_STRING;
		
		Element stateElem = new Element(new QName("state", "http://www.ws4d.org/"), xsString);
		
		setOutput(stateElem);
		
	}
	
	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue,
			CredentialInfo credentialInfo) throws InvocationException,
			CommunicationException {
		
		ParameterValue result = createOutputValue();
		
		BulbHandler h = BulbHandler.getInstance();
		
		ParameterValueManagement.setString(result, "state", (h == null) ? "err" : h.getStateAsString());
		
		return result;
	}

}
