package org.ws4d.mobile.authentication.lightbulb;

import framboos.OutPin;
import framboos.util.HardReset;

public class BulbHandler {
	
	private static BulbHandler mInstance = null;
	private OutPin mOutPin = null;
	private boolean mState;
	
	private BulbHandler() {
		this(0);
	}
	
	private BulbHandler(int i) {
		HardReset.hardResetPin(i);
		mOutPin = new OutPin(i);
		mState = false;
	}
	
	public static void init(int i) {
		if (mInstance == null)
			mInstance = new BulbHandler(i);
	}
	
	public static BulbHandler getInstance() {
		return mInstance;
	}
	
	public OutPin getBulbPin() {
		if (mInstance == null)
			return null;
		return mInstance.mOutPin;
	}
	
	public boolean isBulbOn() {
		return mState;
	}
	public String getStateAsString() {
		if (mState)
			return "on";
		else
			return "off";
	}
	
	public void setBulbOn(boolean s) {
		mState = s;
	}

}
