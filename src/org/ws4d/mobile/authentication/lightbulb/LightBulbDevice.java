package org.ws4d.mobile.authentication.lightbulb;

import org.ws4d.java.communication.DPWSCommunicationManager;
import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.service.DefaultDevice;
import org.ws4d.java.types.EndpointReference;
import org.ws4d.java.types.LocalizedString;
import org.ws4d.java.types.QName;
import org.ws4d.java.types.QNameSet;
import org.ws4d.java.types.URI;

import framboos.OutPin;

public class LightBulbDevice extends DefaultDevice {
	
	@Override
	protected void deviceIsStopping() {
		BulbHandler h = BulbHandler.getInstance();
		OutPin pin = (h==null) ? null : h.getBulbPin();
		if (pin != null)
			pin.close();
	}

	public LightBulbDevice() {
		super(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
		/* in case pin is still open, force close it before reopening */
		this.setFirmwareVersion("5");
		
		QNameSet portTypes = new QNameSet(new QName("LightbulbDevice", "http://www.ws4d.org/"));
		this.setPortTypes(portTypes);
		
		this.addFriendlyName("en-US", "MyShisselBulb");
		this.addFriendlyName(LocalizedString.LANGUAGE_DE, "MeineKrasseGluehbirne");
		
		this.addManufacturer(LocalizedString.LANGUAGE_EN, "Light Bulbs Inc.");
		this.addManufacturer(LocalizedString.LANGUAGE_DE, "Gluehbirnen GmbH");
		
		this.addModelName(LocalizedString.LANGUAGE_DE, "12345");
		this.addModelName(LocalizedString.LANGUAGE_EN, "12345");
		
		this.setEndpointReference(new EndpointReference(new URI(AuthenticationEngine.getSafeDefaultOwnerID())));
		
		
	}
	
	
	
}
