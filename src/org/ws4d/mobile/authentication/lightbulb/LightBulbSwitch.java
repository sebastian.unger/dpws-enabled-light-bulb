package org.ws4d.mobile.authentication.lightbulb;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.incubation.wscompactsecurity.securitytokenservice.SecureOperation;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.schema.Type;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;
import org.ws4d.java.util.Log;

import framboos.OutPin;

public class LightBulbSwitch extends SecureOperation{

	private boolean error = false;

	public LightBulbSwitch() {
		super("LightBulbSwitch", new QName("LightBulbService", "http://www.ws4d.org/"));

		Type xsString = SchemaUtil.TYPE_STRING;

		Element stateElem = new Element(new QName("state", "http://www.ws4d.org/"), xsString);

		setInput(stateElem);
		setOutput(stateElem);

		this.setAuthenticatedOnly(true);
		this.setEncryptedOnly(true);
		this.setAuthorizedOnly(true);
	}



	@Override
	protected ParameterValue invokeImpl(ParameterValue parameterValue,
			CredentialInfo credentialInfo) throws InvocationException,
			CommunicationException {

		/* *********************************
		 *            <MAGIC>
		 * ******************************* */
		enforceSecurity(parameterValue);
		/* *********************************
		 *           </MAGIC>
		 * ******************************* */

		String newState = ParameterValueManagement.getString(parameterValue, "state");

		ParameterValue result = createOutputValue();

		BulbHandler h = BulbHandler.getInstance();

		if (h == null)
			this.error = true;
		else {
			OutPin bulbPin = h.getBulbPin();
			if (newState.toLowerCase().equals("off")) {
				h.setBulbOn(false);
				if (bulbPin != null) {
					bulbPin.setValue(false);
				}
				this.error = false;
			} else if (newState.toLowerCase().equals("on")) {
				h.setBulbOn(true);
				if (bulbPin != null) {
					bulbPin.setValue(true);
				}
				this.error = false;
			} else {
				this.error = true;
			}
		}

		if (this.error) {
			ParameterValueManagement.setString(result, "state", "--error in request--");

		} else {
			ParameterValueManagement.setString(result, "state", h.getStateAsString());
			Log.info("Will turn bulb " + h.getStateAsString());
		}

		return result;
	}

}
