package org.ws4d.mobile.authentication.lightbulb;

import org.ws4d.java.incubation.wscompactsecurity.authentication.engine.AuthenticationEngine;
import org.ws4d.java.incubation.wscompactsecurity.authorization.engine.AuthorizationEngine;
import org.ws4d.java.util.Log;

import framboos.InPin;
import framboos.OutPin;
import framboos.util.HardReset;

public class ButtonHandler {
	private ButtonHandler() {
		this(1);
	}

	private static InPin mInPin = null;
	private PinWatcher mPinWatcher = null;

	private class PinWatcher extends Thread {

		private int mCheckInterval;
		private int mTimeOut;

		private long timeStamp=0;

		public PinWatcher(int timeOut, int checkInterval) {
			this.mTimeOut = timeOut;
			this.mCheckInterval = checkInterval;
		}

		@Override
		public void run() {
			while (true) {
				if (mInPin.getValue()) {
					if (timeStamp == 0) {
						timeStamp = System.currentTimeMillis();
					} else {
						if ((System.currentTimeMillis() - timeStamp) > mTimeOut) {
							timeStamp = 0;
							// TODO: work with callback interface (for a reason I don't know yet...)
							AuthenticationEngine ae = AuthenticationEngine.getInstance();
							if (ae != null) {
								ae.removeAllSecurityAssociations(false);
								/* Don't restart service - will be handled by AuthzEngn.rst() */
							}
							ae.reset();
							/* give optical feedback */
							BulbHandler bh = BulbHandler.getInstance();
							if (bh != null) {
								boolean state = bh.isBulbOn();
								OutPin bp = bh.getBulbPin();
								bp.setValue(!state);
								try {
									Thread.sleep(200);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
								bp.setValue(state);
							}
							Log.info("Cleared Security Associations!");
							AuthorizationEngine authzengine = AuthorizationEngine.getInstance();
							if (authzengine != null) {
								authzengine.clearDatabase();
								authzengine.reset();
							} else {
								Log.warn("No Authorization Engine instance!");
							}
							Log.info("Cleared Authorization engine");
						}
					}
				} else {
					timeStamp = 0;
				}
				try {
					Thread.sleep(mCheckInterval);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	public static ButtonHandler instance = null;

	public static ButtonHandler getInstance() {
		return instance;
	}

	public static void init(int i) {
		if (instance == null) {
			instance = new ButtonHandler(i);
		}
	}

	private ButtonHandler(int pinNumber) {
		/* in case pin is still open, force close it before reopening */
		HardReset.hardResetPin(pinNumber);
		mInPin = new InPin(pinNumber);
		mPinWatcher = new PinWatcher(5000, 250);
		mPinWatcher.start();
	}

	public static Boolean isPinPressed() {
		if (mInPin == null)
			return null;
		return Boolean.valueOf(mInPin.getValue());
	}

}
